-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Sep 2020 pada 13.34
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sibe4`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_service`
--

CREATE TABLE `daftar_service` (
  `no_pendaftaran` int(20) NOT NULL,
  `no_antrian` varchar(10) NOT NULL,
  `no_polisi` varchar(15) NOT NULL,
  `no_mesin` varchar(20) NOT NULL,
  `nama_pemilik` varchar(35) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `kendaraan` varchar(20) NOT NULL,
  `keluhan` varchar(220) NOT NULL,
  `nama_mekanik` varchar(20) NOT NULL,
  `saran_mekanik` varchar(50) NOT NULL,
  `tgl_daftar` varchar(20) NOT NULL,
  `status` enum('1','2','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar_service`
--

INSERT INTO `daftar_service` (`no_pendaftaran`, `no_antrian`, `no_polisi`, `no_mesin`, `nama_pemilik`, `no_hp`, `kendaraan`, `keluhan`, `nama_mekanik`, `saran_mekanik`, `tgl_daftar`, `status`) VALUES
(1, '', 'B 1234 CDE', '', 'Bambang', '', 'Yamaha Mio', 'service', '', '', '2017-06-08', '2'),
(2, '', 'F 456 GHJ', '', 'Samsul', '', 'Honda Beat', 'Ganti Oli', '', '', '2017-06-16', '0'),
(3, '', 'AD1111QA', '', 'ter', '', 'beat', 'rawat rutin', '', '', '05-08-2020', '2'),
(4, '', 'AG 1111 AS', '', 'Agung', '', 'Honda Beats ', 'Service', '', '', '15-09-2020', '1'),
(5, '', 'AD99002CH', '', 'Fadil', 'Honda Supra X', '082198268968', '', '', '', '17/09/2020', '1'),
(7, '', 'AF 998CV', '', 'Ari', 'Honda BeatX', '0811223453', '', '', '', '17/09/2020', '1'),
(8, '', 'AD8899BN', '', 'gilang', 'Honda Tiger 2000', '0811111118', '', '', '', '17/09/2020', '1'),
(9, '', '', '', '', '', '', '', '', '', '', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gudang`
--

CREATE TABLE `gudang` (
  `id_barang` int(20) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `stok` int(5) NOT NULL,
  `harga` int(25) NOT NULL,
  `satuan` enum('pcs','set') NOT NULL,
  `kode_gudang` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gudang`
--

INSERT INTO `gudang` (`id_barang`, `kode_barang`, `nama_barang`, `stok`, `harga`, `satuan`, `kode_gudang`) VALUES
(4, '123', 'test', 9, 2000, 'pcs', '2'),
(5, '456', 'test2', 10, 1000, 'pcs', '4'),
(6, '12312312', 'yuhu', 5, 500000, 'pcs', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jasa`
--

CREATE TABLE `jasa` (
  `id` int(11) NOT NULL,
  `nama_jasa` varchar(20) NOT NULL,
  `harga` int(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jasa`
--

INSERT INTO `jasa` (`id`, `nama_jasa`, `harga`) VALUES
(1, 'Ganti Sparepart', 0),
(2, 'Jasa Service', 10000),
(3, 'Pengecekan', 5000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesan_barang`
--

CREATE TABLE `pesan_barang` (
  `id_pesan` int(11) NOT NULL,
  `gudang` varchar(50) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `qty` int(10) NOT NULL,
  `tgl` date NOT NULL,
  `pemesan` varchar(30) NOT NULL,
  `status` enum('menunggu','dikirim','ditolak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pesan_barang`
--

INSERT INTO `pesan_barang` (`id_pesan`, `gudang`, `nama_barang`, `qty`, `tgl`, `pemesan`, `status`) VALUES
(4, '2', '4', 1, '2017-07-06', 'Administrator', 'dikirim');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `no_pendaftaran` varchar(25) NOT NULL,
  `tgl_service` date NOT NULL,
  `barang` varchar(30) NOT NULL,
  `jasa` varchar(50) DEFAULT NULL,
  `montir` varchar(20) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `harga` int(10) NOT NULL,
  `storename` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `no_pendaftaran`, `tgl_service`, `barang`, `jasa`, `montir`, `jumlah`, `harga`, `storename`) VALUES
(1, '2', '2020-09-15', ' Hanya Jasa', 'Ganti Sparepart', 'Administrator', 1, 10000, 'Bengkel'),
(2, '1', '2020-09-15', ' Hanya Jasa', 'Ganti Sparepart', 'Administrator', 1, 10000, 'Bengkel'),
(3, '3', '2020-09-15', ' test', 'Ganti Sparepart', 'Montir', 1, 12000, 'Bengkel'),
(4, '3', '2020-09-15', ' Hanya Jasa', 'Ganti Sparepart', 'Montir', 1, 5000, 'Bengkel'),
(5, '3', '2020-09-15', ' Hanya Jasa', 'Ganti Sparepart', 'Montir', 1, 0, 'Bengkel');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` enum('1','2','3','4') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `role`) VALUES
(1, 'Administrator', 'admin', 'admin', '1'),
(2, 'Admin Gudang1', 'gudang1', 'gudang1', '2'),
(3, 'Montir', 'montir', 'montir', '3'),
(4, 'Admin Gudang2', 'gudang2', 'gudang2', '2'),
(5, 'Kasir', 'kasir', 'kasir', '4');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `daftar_service`
--
ALTER TABLE `daftar_service`
  ADD PRIMARY KEY (`no_pendaftaran`);

--
-- Indeks untuk tabel `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `jasa`
--
ALTER TABLE `jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pesan_barang`
--
ALTER TABLE `pesan_barang`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `daftar_service`
--
ALTER TABLE `daftar_service`
  MODIFY `no_pendaftaran` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id_barang` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `jasa`
--
ALTER TABLE `jasa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pesan_barang`
--
ALTER TABLE `pesan_barang`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
