<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">

  <title>AHASS LESTARI KEDIRI</title>
  <!--Meta tags-->
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <!-- Add your Fav icon here -->
  <link rel="icon" type="image/png" href="<?php echo base_url('assets'); ?>/images/favicon.png">

  <!-- Styles  -->  
 <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('landing'); ?>/css/themify-icons.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('landing'); ?>/css/animate.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('landing'); ?>/css/style.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('landing'); ?>/css/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('landing'); ?>/css/owl.theme.default.min.css">
 
<script crossorigin="anonymous" integrity="sha384-xBuQ/xzmlsLoJpyjoggmTEz8OWUFM0/RC5BsqQBDX2v5cMvDHcMakNTNrHIW2I5f" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url('landing'); ?>/js/bootstrap-datetimepicker.min.js"></script>

<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css">

<!-- Include Bootstrap Datepicker -->
 <link rel="stylesheet" href="<?php echo base_url('landing'); ?>/css/bootstrap-datetimepicker.min.css" type="text/css" media="all" />



	    <style>
		<!-- .datepicker{z-index:1151;} -->
	    </style>
	    <!--some script for datepicker-->
	    <script>
		(function($){
    $(function(){

        $('#id_4').datetimepicker({
            "allowInputToggle": true,
            "showClose": true,
            "showClear": true,
            "showTodayButton": true,
            "format": "DD/MM/YYYY",
			"locale":'id',
        });
		
		
    });
	
})(jQuery);
	    </script>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <!-- Header Start-->
  <nav id="navbar" class="navbar fixed-top navbar-expand-lg navbar-header navbar-mobile">
    <div class="navbar-container container">
      <!-- Logo -->
      <div class="navbar-brand">
        <a class="navbar-brand-logo" href="#top">
          <img src="<?php echo base_url('landing'); ?>/images/logo.png" alt="logo">
        </a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="ti-layout-grid2"></span>
      </button>
      <!-- Menu -->
      <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav menu-navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#top">
              <p class="nav-link-menu">Home</p>
            </a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" href="#servis">
              <p class="nav-link-menu">Layanan Servis</p>
            </a>
          </li>              
          <li class="nav-item">
            <a class="nav-link" href="#daftar">
              <p class="nav-link-menu">Daftar Servis</p>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link learn-more-btn btn-invert" href="<?=base_url('login/login');?>">
              LOGIN</a>
          </li>
        </ul>
      </div>
      <!-- End Menu-->
    </div>
  </nav>
  <!-- Header End -->

  <div id="top"></div>
  <!-- Banner Start-->
  <div class="wrapper">
    <div class="header" >
      <div class="container header-container">
	  <img src="<?php echo base_url('landing'); ?>/images/banner1.png">
      
         
      </div>
    </div>
    <!-- Banner End-->
    <!-- Service Section Start-->
    <div id="servis"></div>

    <div class="services-section">
      <div class="container">
        <div class="col-lg-12 col-sm-12 about-title-section">
          <p class="about-subtitle">Servis Pasti Dari Yang Ahli</p>
          <h2 class="about-title">Layanan Servis</h2>
          <p class="about-text mb-5">Layanan AHASS yang menyediakan segala kebutuhan perawatan
dan perbaikan sepeda motor Honda Anda.</p>
          <div class="row service-list  wow slideInUp">
            <div class="col-lg-4 col-md-6 col-sm-12">
              <div class="service-box">
                <span class="icon"><img src="<?php echo base_url('landing'); ?>/images/services/001-booking-servis.png" alt="car part"></span>
                <h5 class="my-4">Layanan Booking Servis</h5>
                <p>Gunakan layanan Booking Servis untuk memilih waktu servis sesuai dengan jadwal luang Anda.</p>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
              <div class="service-box">
                <span class="icon"><img src="<?php echo base_url('landing'); ?>/images/services/002-servis.png" alt="car part"></span>
                <h5 class="my-4">Pit Express</h5>
                <p>Layanan khusus untuk penggantian oli & fast moving parts lainnya. </p>
              </div>
            </div>                      
            <div class="col-lg-4 col-md-6 col-sm-12">
              <div class="service-box">
                <span class="icon"><img src="<?php echo base_url('landing'); ?>/images/services/006-insurance-1.png" alt="car part"></span>
                <h5 class="my-4">Jaminan After Servis</h5>
                <p>Layanan Servis AHASS memberikan jaminan after sales yang baik.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Service Section End-->

   
    <!-- Contact Section Start-->
    <div id="daftar"></div>

    <div class="contact-section">
      <div class="container contact-container">
        <div class="col-md-6 col-sm-12 wow slideInLeft">
          <form class="contact-form" method="post" action="<?php echo base_url(); ?>bookingService">

            <div class="form-group">
              <label>Nama Pemilik</label>
              <input type="text" name="nama_pemilik" class="form-control" placeholder="Masukkan nama lengkap ...">
            </div>
            <div class="form-group">
              <label>No. Handphone</label>
              <input type="text" name="phone" class="form-control" placeholder="Masukkan No Telp ...">
            </div>
            <div class="form-group">
              <label>Merk Kendaraan</label>
              <input type="text" name="kendaraan" class="form-control" placeholder="Masukkan Merk Kendaraan ...">
            </div>
            <div class="form-group">
              <label>No. Polisi</label>
             <input type="text" name="no_polisi" class="form-control" placeholder="Masukkan Plat Kendaraan AG ...">
            </div>
			
			<div class="form-group">
                        <label for="id_end_time">Pilih Tanggal Servis</label>
                        <div class="input-group date" id="id_4">
                            <input type="text" class="form-control" name="tgl_daftar"/>
                            <div class="input-group-addon input-group-append">
                                <div class="input-group-text">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>				
            <div class="form-group mb-0">
              <button type="submit" class="btn learn-more-btn">Submit</button>
            </div>
            <div class="alert d-none alert-success mt-3 mb-0"></div>
          </form>
        </div>
        <div class="col-md-5 offset-md-1 col-sm-12 contact-header-img wow slideInRight">
          <div class="products-title">
            <h2>Daftar Servis</h2>
            <p class="mb-5">Gunakan layanan Booking Servis untuk memilih waktu servis sesuai dengan jadwal luang Anda.</p>
          </div>
          <img src="<?php echo base_url('landing'); ?>/images/contact.svg" alt="contact banner">
        </div>
      </div>
    </div>
    <!-- Contact Section End-->
    <!-- Footer Section Start-->
    <div class="footer-section">
      <div class="container footer-container">
        <div class="col-lg-3 col-md-6 footer-logo">
          <img src="<?php echo base_url('landing'); ?>/images/logo-white.png" alt="footer logo">
          <p class="footer-susection-text">One HEART</p>
        </div>
        <div class="col-lg-3 col-md-6 footer-subsection">
          <div class="footer-subsection-2-1">
           
          </div>
        </div>
        <div class="col-lg-3 col-md-6 footer-subsection">
          
        </div>
        <div class="col-lg-7 col-md-2 footer-subsection">
          <div class="footer-subsection-2-2">
            <h3 class="footer-subsection-title">AHASS Surya Lestari Motor</h3>
          <ul class="footer-subsection-list">
            <li>Jl. Raya Kediri-Tulungagung No.5 RT.004 RW.005</li>
            <li>Kab. Kediri, Jawa Timur</li>
          </ul>
          </div>
		  <p>&copy; 2020 All Rights Reserved.</p>
        </div>
      </div>
    </div>
    <!-- Footer Section End-->
    <div id="preloader">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
  </div>
  <!-- Javascript files-->
	
  
  <script src="<?php echo base_url('landing'); ?>/js/jquery.min.js"></script>
  <script src="<?php echo base_url('landing'); ?>/js/popper.min.js"></script>
  <script src="<?php echo base_url('landing'); ?>/js/smoothscroll.js"></script>
  <script src="<?php echo base_url('landing'); ?>/js/wow.min.js"></script>
  <script crossorigin="anonymous" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url('landing'); ?>/js/owl.carousel.js"></script>
  <script src="<?php echo base_url('landing'); ?>/js/custom.js"></script>
  <!-- Javascript files-->
</body>

</html>
