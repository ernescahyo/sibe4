<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
		
	}

	public function save()
	{	
		$no_polisi=$this->input->post('no_polisi');
		$nama_pemilik=$this->input->post('nama_pemilik');
		$kendaraan=$this->input->post('kendaraan');
		$no_hp=$this->input->post('phone');
		$tgl_daftar=$this->input->post('tgl_daftar');
		//	$this->load->view('/landing_page/index');
		$this->load->model('Data_model');		
		$this->Data_model->booking_save($no_polisi,$nama_pemilik,$no_hp,$kendaraan,$tgl_daftar);
		
		$this->load->view('/landing_page/index');
	}
	
	
	
	public function login()
	{	
		if($this->session->userdata('role') == 2)
		{				
			redirect('dataGudang');
		}
		else if($this->session->userdata('role') == 3)
		{				
			redirect('dataTransaksi');
		}
		else if($this->session->userdata('role') == 4)
		{				
			redirect('daftarService');
		}else if($this->session->userdata('role') == 1){				
			redirect('dashboard');
		}else{
			$this->load->view('login');
		}
	}
	
	public function cek_login()
	{
		$u = $this->input->post('u');
		$p = $this->input->post('p');
		$cek_login = $this->Data_model->cek_user($u, $p);
		if($cek_login->num_rows() <> 0)
		{
			$this->session->set_userdata('nama', $cek_login->row('nama'));
			$this->session->set_userdata('role', $cek_login->row('role'));
			$this->session->set_userdata('id_user', $cek_login->row('id_user'));
			if($cek_login->row('role') == 2)
			{				
				redirect('dataGudang');
			}
			else if($cek_login->row('role') == 3)
			{				
				redirect('dataTransaksi');
			}
			else if($cek_login->row('role') == 4)
			{				
				redirect('daftarService');
			}else{				
				redirect('dashboard');
			}
		}else{
			echo "<script>alert('Username/Password salah!');window.location.href = '".base_url("login")."';</script>";
		}
	}
	

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */